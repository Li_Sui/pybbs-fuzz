# Pybbs
- version 5.2.1
- github link: https://github.com/tomoya92/pybbs

### MYSQL
Requires a MYSQL database to be running. Create a user: `root`, password: `password`.
You can modify this by opening pybbs.war, update the file `/WEB-INF/classes/application-dev.yml`. pybbs will create a self-named schema with required tables on startup if not present already.

### Tomcat
- apache-tomcat-8.5.71
- admin  should contain the following roles: `manager-gui,manager-script,manager-jmx`

### Changes
Added 5 new classes
- `co.yiiu.pybbs.fuzzInterceptor.FeedbackInterceptor`
- `co.yiiu.pybbs.service.FeedbackService`
- `co.yiiu.pybbs.service.ApplicationPackageParamMissingException`
- `co.yiiu.pybbs.service.NotFoundException`

Added a new dependency in pom.xml
```
<dependency>
    <groupId>nz.ac.wgtn.cornetto</groupId>
    <artifactId>cornetto-rt</artifactId>
    <version>2.0.2</version>
</dependency>
```

Modified following classes:
<!--Modified 2 files as they created error on start up
- `src/main/resources/templates/theme/default/layout/header.ftl` removed `${site.name!}` at line 3
- `src/main/resources/templates/theme/default/components/welcome.ftl` removed `${site.intro!}` at line 3-->
- `co.yiiu.pybbs.config.WebMvcConfig` to register the FeedbackInterceptor. at line 33,47

### Server deployment

1. checkout: https://bitbucket.org/jensdietrich/webfuzzer/
2. cd into webfuzz-rt and build with `mvn install`
3. check that the correct version referenced in the pom is installed
4. build project with `mvn package`
5. run generated jar in `/target` with `java -javaagent:tools/aspectjweaver-1.9.6.jar -jar target/pybbs.jar` .

### Authentication
- `nz.ac.wgtn.cornetto.authenticators.PybbsAuthenticator`
- admin username&password `admin`&`123123`

### Known server issues (500)
freemarker.core.InvalidReferenceException: https://github.com/tomoya92/pybbs/issues/160

### Known injection attacks

```
/search?keyword=
admin/comment/list?startDate=
admin/user/list?username=
admin/sensitive_word/list?word=
admin/tag/list?name=
```

https://github.com/tomoya92/pybbs/issues/158
