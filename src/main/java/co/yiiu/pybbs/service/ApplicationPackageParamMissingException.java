package co.yiiu.pybbs.service;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception to set return type in FeedbackService.
 * @author jens dietrich
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason="Request parameter applicationpackages is missing")
public class ApplicationPackageParamMissingException extends RuntimeException  {
    public ApplicationPackageParamMissingException() {
    }

    public ApplicationPackageParamMissingException(String message) {
        super(message);
    }

    public ApplicationPackageParamMissingException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationPackageParamMissingException(Throwable cause) {
        super(cause);
    }

    public ApplicationPackageParamMissingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}